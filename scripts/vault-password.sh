#!/usr/bin/env bash

set -e

path="$(dirname "$(realpath "${BASH_SOURCE[0]}" || true)")"

cd "${path}/.."

sops --decrypt "./secrets/vault-password.bin"
