#!/usr/bin/env bash

set -e

shopt -s globstar

path="$(dirname "$(realpath "${BASH_SOURCE[0]}" || true)")"

cd "${path}/.."

yamllint --strict ./src/**/*.yml

ansible-lint --strict
