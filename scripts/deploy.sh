#!/usr/bin/env bash

set -e

path="$(dirname "$(realpath "${BASH_SOURCE[0]}" || true)")"

cd "${path}/.."

ansible-playbook --inventory "./src/inventories/main/hosts.yml" "$@" "./src/playbooks/main.yml"
