{
  imports = [
    ./devshells
    ./formatter.nix
  ];

  systems = [
    "x86_64-linux"
  ];
}
