{
  perSystem = {pkgs, ...}: {
    devShells = {
      "interpreter" = pkgs.mkShell {
        packages = [
          (
            pkgs.python3.withPackages (
              pythonPackages: [
                pythonPackages.docker
              ]
            )
          )
        ];
      };
    };
  };
}
